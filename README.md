## Atlassian UI Challenge

### How to run

- Install all dependencies by `npm install`
- Run a mock server `npm run server`
- Run a main server `npm start`
- Type 'localhost:3000'

### Check lint and test
- `npm run lint` // Check all the linting
- `npm run test:ci` // Will run the test cases