import React from 'react';
import { shallow } from 'enzyme';
import { Grid } from 'react-flexbox-grid';

import { Space } from './index';
import { Table } from '../../components/table/index';
import { Loader } from '../../components/loader/index';

let wrapper;
let props;
const assets = { assets: [{ name: 'bar' }] };

describe('on load of Panel', () => {
  beforeAll(() => {
    props = {
      fields: [{
        fields: {
          title: 'My First Space',
          description: 'This space was created by Alanna Atlassian as an example of how to create a space and relate assets and entries to it.'
        },
        sys: {
          id: 'yadj1kx9rmg0',
          type: 'Space',
          createdAt: '2015-05-18T11:29:46.809Z',
          createdBy: '4FLrUHftHW3v2BLi9fzfjU',
          updatedAt: '2015-05-18T11:29:46.809Z',
          updatedBy: '4FLrUHftHW3v2BLi9fzfjU'
        }
      }],
      dispatch: jest.fn(),
      spaceDataById: {},
      entries: [],
      users: {},
      match: {
        params: {}
      }
    };
    wrapper = shallow(<Space {...props} />);
  });

  it('should call dispatch event', () => {
    expect(props.dispatch).toHaveBeenCalled();
  });

  it('should render a Grid', () => {
    expect(wrapper.find(Grid).length).toBe(1);
  });

  describe('on getting space id from params', () => {
    beforeAll(() => {
      props.match.params = {
        spaceId: 'foo'
      };

      props.spaceDataById = {
        description: 'Foo bar',
        title: 'Foo'
      };

      wrapper = shallow(<Space {...props} />);
    });

    it('should call dispatch event', () => {
      expect(props.dispatch).toHaveBeenCalled();
    });

    it('should render a Grid', () => {
      expect(wrapper.find('p').text()).toContain(props.spaceDataById.description);
    });

    describe('on getting user data from parmas', () => {
      beforeAll(() => {
        props.users = {
          userId: {
            name: 'foo'
          }
        };

        wrapper.setProps({ users: { userId: { name: 'foo' } } });
      });

      it('should call dispatch event', () => {
        expect(props.dispatch.mock.calls).toMatchSnapshot();
      });

      it('should not display button', () => {
        expect(wrapper.find('button').length).toBe(0);
      });

      it('should display Loader', () => {
        expect(wrapper.find(Loader).length).toBe(2);
      });

      describe('on getting entries data from params', () => {
        beforeAll(() => {
          wrapper.setProps({ entries: [{ name: 'foo' }] });
          wrapper.setState({ items: [{ name: 'foo' }] });
        });

        it('should display button', () => {
          expect(wrapper.find('button').length).toBe(2);
        });

        it('should display table', () => {
          expect(wrapper.find(Table).length).toBe(1);
        });

        describe('on click of button of assets', () => {
          beforeAll(() => {
            wrapper.setProps(assets);
            wrapper.find('button').at(1).simulate('click');
          });

          it('should set the state of assets', () => {
            expect(wrapper.state()).toMatchObject({ items: assets.assets });
          });
        });
      });
    });
  });
});
