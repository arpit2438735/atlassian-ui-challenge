import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Grid, Row, Col } from 'react-flexbox-grid';

import {
  fetchAllSpace,
  fetchSpaceBySpaceId
} from '../../actions/space';
import fetchUserDetails from '../../actions/user';
import fetchEntriesForSpace from '../../actions/entries';
import fetchAssetsForSpace from '../../actions/assets';

import { Panel } from '../../components/panel';
import { Table } from '../../components/table';
import { Loader } from '../../components/loader';

export class Space extends PureComponent {
    static defaultProps = {
      dispatch: PropTypes.func,
      fields: PropTypes.instanceOf(Array),
      spaceDataById: PropTypes.instanceOf(Object),
      match: PropTypes.objectOf({
        params: PropTypes.objectOf({
          spaceId: PropTypes.string
        })
      }),
      entries: PropTypes.instanceOf(Array),
      users: PropTypes.instanceOf(Object)
    };

    static propTypes= {
      dispatch: PropTypes.func,
      fields: PropTypes.instanceOf(Array),
      spaceDataById: PropTypes.instanceOf(Object),
      match: PropTypes.objectOf({
        params: PropTypes.objectOf({
          spaceId: PropTypes.string
        })
      }),
      entries: PropTypes.instanceOf(Array),
      users: PropTypes.instanceOf(Object)
    };

    static mapArray (fields) {
      return fields.map(field => ({
        id: field.sys.id,
        title: field.fields.title
      }));
    }

    state = {
      items: []
    };

    constructor () {
      super();

      this.updateTableWith = this.updateTableWith.bind(this);
    }

    componentDidMount () {
      const { match, dispatch } = this.props;
      const { spaceId } = match.params;

      dispatch(fetchAllSpace());

      if (spaceId) {
        dispatch(fetchSpaceBySpaceId({ spaceId }));
        dispatch(fetchUserDetails());
      }
    }

    componentWillReceiveProps (props) {
      const { users, entries } = this.props;
      if (!Object.keys(users).length && Object.keys(props.users).length) {
        props.dispatch(fetchEntriesForSpace());
        props.dispatch(fetchAssetsForSpace());
      }

      if (!entries.length && props.entries && props.entries.length) {
        this.setState({
          items: [...props.entries]
        });
      }
    }

    updateTableWith (type) {
      const { props } = this;

      this.setState({
        items: [...props[type]]
      });
    }

    render () {
      const {
        fields, spaceDataById, entries, users
      } = this.props;
      const { items } = this.state;

      return (
        <div>
          <Grid>
            <Row>
              <Col xs={12} lg={2}>
                <Panel fields={Space.mapArray(fields)} />
              </Col>
              <Col xs={12} lg={10}>
                { Object.keys(spaceDataById).length
                  ? (
                    <div>
                      <h1>{ spaceDataById.title }</h1>
                      <Col xs={6} lg={6} className='m-b-30'>
                        <p className='border padding-10'>
                          { spaceDataById.description }
                        </p>
                      </Col>

                      <Col xs={12}>
                        <Row>
                          {entries.length ? (
                            <Col xs={6} className='m-b-30'>
                              <button className='primary-button m-r-10' type='button' onClick={() => this.updateTableWith('entries')}>Entries</button>
                              <button className='primary-button' type='button' onClick={() => this.updateTableWith('assets')}>Assets</button>
                            </Col>
                          ) : <Loader />}
                        </Row>

                        <Row>
                          {items.length ? <Table items={items} users={users} /> : <Loader />}
                        </Row>
                      </Col>
                    </div>
                  )
                  : <h1>Welcome to space</h1>}
              </Col>
            </Row>
          </Grid>
        </div>
      );
    }
}

const mapStateToProps = ({
  space, user, entries, assets
}) => ({
  fields: space.fields,
  spaceDataById: space.spaceDataById,
  users: user.details,
  entries: entries.items,
  assets: assets.items
});

export default connect(mapStateToProps)(Space);
