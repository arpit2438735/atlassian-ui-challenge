import React  from 'react';
import { Row, Col } from 'react-flexbox-grid';

import svg from '../../icons/error-illustration.svg';
import './style.css';

const NotFound = () => (
  <Row className='error'>
    <Col xs={12}>
      <Row middle='xs' className='full-height'>
        <Col xs={12} className='center-xs'>
          <img src={svg} alt='NOT FOUND' />
          <h1>That link has no power here</h1>
          <p>
Return to the
            <a href='/'>dashboard</a>
            {' '}
page.
          </p>
        </Col>
      </Row>
    </Col>
  </Row>
);

export default NotFound;
