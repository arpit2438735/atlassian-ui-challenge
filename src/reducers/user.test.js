import reducer from './user';

import {
  FETCH_USER_DETAILS_SUCCESS
} from '../constant/user';

const successResponse = {
  items: [{
    fields: {
      name: 'Alana Atlassian',
      role: 'Author'
    },
    sys: {
      id: '4FLrUHftHW3v2BLi9fzfjU',
      type: 'User'
    }
  }, {
    fields: {
      name: 'John Doe',
      role: 'Editor'
    },
    sys: {
      id: '4FLrUHftHW3v2BLi9fzfjU2',
      type: 'User'
    }
  }]
};

const expectedUserObject = {
  '4FLrUHftHW3v2BLi9fzfjU': {
    name: 'Alana Atlassian',
    role: 'Author'
  },
  '4FLrUHftHW3v2BLi9fzfjU2': {
    name: 'John Doe',
    role: 'Editor'
  }
};

describe('on call of user reducer', () => {
  it('should set user by userid in object', () => {
    const onSuccessAction = {
      type: FETCH_USER_DETAILS_SUCCESS,
      payload: successResponse
    };

    expect(reducer({}, onSuccessAction).details).toEqual(expectedUserObject);
  });
});
