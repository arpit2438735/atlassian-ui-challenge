import {
  FETCH_SPACE_ASSETS_SUCCESS
} from '../constant/assets';

const initialState = {
  items: []
};

const massageAssetsDetails = entries => entries.map((entry) => {
  const { sys, fields } = entry;

  const { title, contentType, fileName } = fields;
  const {
    createdBy, updatedBy, updatedAt, id
  } = sys;

  return {
    id,
    title,
    contentType,
    fileName,
    createdBy,
    updatedBy,
    updatedAt
  };
}, {});

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_SPACE_ASSETS_SUCCESS: {
      return  { ...state, items: [...massageAssetsDetails(payload.items)] };
    }
    default: {
      return state;
    }
  }
};
