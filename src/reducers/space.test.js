import reducer from './space';

import {
  FETCH_SPACES_SUCCESS,
  FETCH_SPACES_FAILURE,
  FETCH_SPACES_BY_SPACE_ID_SUCCESS,
  FETCH_SPACES_BY_SPACE_ID_FAILURE
} from '../constant/space';

const successResponse = {
  items: [
    {
      data: 'foo'
    }
  ]
};

const successResponseOfFieldDataBySpaceId = {
  fields: {
    foo: 'bar'
  }
};

const failureResponse = {
  message: 'bar'
};

describe('on call of space reducer', () => {
  it('space object contain data now on getting success response', () => {
    const onSuccessAction = {
      type: FETCH_SPACES_SUCCESS,
      payload: successResponse
    };

    expect(reducer({}, onSuccessAction).fields).toEqual(successResponse.items);
  });

  it('loading should be false on getting success response', () => {
    const onSuccessAction = {
      type: FETCH_SPACES_SUCCESS,
      payload: successResponse
    };

    expect(reducer({ loading: true }, onSuccessAction).loading).toBe(false);
  });

  it('loading should be false on getting failure response', () => {
    const onFailureAction = {
      type: FETCH_SPACES_FAILURE,
      payload: failureResponse
    };

    expect(reducer({ loading: true }, onFailureAction).loading).toBe(false);
  });

  it('set failure message on getting failure response', () => {
    const onFailureAction = {
      type: FETCH_SPACES_FAILURE,
      payload: failureResponse
    };

    expect(reducer({ loading: true }, onFailureAction).message).toEqual(failureResponse.message);
  });

  it('set field data for particular space id on getting success response', () => {
    const onSuccessAction = {
      type: FETCH_SPACES_BY_SPACE_ID_SUCCESS,
      payload: successResponseOfFieldDataBySpaceId
    };

    expect(reducer({}, onSuccessAction).spaceDataById)
      .toEqual(successResponseOfFieldDataBySpaceId.fields);
  });

  it('set message on getting failure response from field data by space id', () => {
    const onFailureAction = {
      type: FETCH_SPACES_BY_SPACE_ID_FAILURE,
      payload: failureResponse
    };

    expect(reducer({}, onFailureAction).message).toEqual(failureResponse.message);
  });
});
