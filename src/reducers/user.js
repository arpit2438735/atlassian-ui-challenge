import {
  FETCH_USER_DETAILS_SUCCESS
} from '../constant/user';

const initialState = {
  details: {}
};

const massageUserDetails = users => users.reduce((userObject, user) => {
  const { sys, fields } = user;
  userObject[sys.id] = {
    name: fields.name,
    role: fields.role
  };

  return userObject;
}, {});

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_USER_DETAILS_SUCCESS: {
      return  { ...state, details: { ...state.details, ...massageUserDetails(payload.items) } };
    }
    default: {
      return state;
    }
  }
};
