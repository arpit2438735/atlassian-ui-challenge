import reducer from './entries';

import {
  FETCH_SPACE_ENTRIES_SUCCESS
} from '../constant/entries';

const successResponse = {
  items: [{
    fields: {
      title: 'Hello, World!',
      summary: 'A Hello World is a common starting point for learning a new language or platform.'
    },
    sys: {
      id: '5KsDBWseXY6QegucYAoacS1',
      type: 'Entry',
      version: 1,
      space: 'yadj1kx9rmg0',
      createdAt: '2015-05-18T11:29:46.809Z',
      createdBy: '4FLrUHftHW3v2BLi9fzfjU',
      updatedAt: '2015-05-18T11:29:46.809Z',
      updatedBy: '4FLrUHftHW3v2BLi9fzfjU'
    }
  },
  {
    fields: {
      title: 'Tutorial 1',
      summary: 'In this tutorial, we will dive deeper into how to leverage the new platform.'
    },
    sys: {
      id: '5KsDBWseXY6QegucYAoacS2',
      type: 'Entry',
      version: 1,
      space: 'yadj1kx9rmg0',
      createdAt: '2015-05-18T11:29:46.809Z',
      createdBy: '4FLrUHftHW3v2BLi9fzfjU',
      updatedAt: '2015-12-18T11:29:46.809Z',
      updatedBy: '4FLrUHftHW3v2BLi9fzfjU2'
    }
  },
  {
    fields: {
      title: 'Tutorial 2',
      summary: 'In this tutorial, we will provide an example deployment.'
    },
    sys: {
      id: '5KsDBWseXY6QegucYAoacS3',
      type: 'Entry',
      version: 1,
      space: 'yadj1kx9rmg0',
      createdAt: '2015-05-18T11:29:46.809Z',
      createdBy: '4FLrUHftHW3v2BLi9fzfjU',
      updatedAt: '2015-12-18T11:29:46.809Z',
      updatedBy: '4FLrUHftHW3v2BLi9fzfjU2'
    }
  }
  ]
};

const expectedUserObject = [{
  createdBy: '4FLrUHftHW3v2BLi9fzfjU', id: '5KsDBWseXY6QegucYAoacS1', summary: 'A Hello World is a common starting point for learning a new language or platform.', title: 'Hello, World!', updatedAt: '2015-05-18T11:29:46.809Z', updatedBy: '4FLrUHftHW3v2BLi9fzfjU'
}, {
  createdBy: '4FLrUHftHW3v2BLi9fzfjU', id: '5KsDBWseXY6QegucYAoacS2', summary: 'In this tutorial, we will dive deeper into how to leverage the new platform.', title: 'Tutorial 1', updatedAt: '2015-12-18T11:29:46.809Z', updatedBy: '4FLrUHftHW3v2BLi9fzfjU2'
}, {
  createdBy: '4FLrUHftHW3v2BLi9fzfjU', id: '5KsDBWseXY6QegucYAoacS3', summary: 'In this tutorial, we will provide an example deployment.', title: 'Tutorial 2', updatedAt: '2015-12-18T11:29:46.809Z', updatedBy: '4FLrUHftHW3v2BLi9fzfjU2'
}];


describe('on call of entries reducer', () => {
  it('should set new entries', () => {
    const onSuccessAction = {
      type: FETCH_SPACE_ENTRIES_SUCCESS,
      payload: successResponse
    };

    expect(reducer({}, onSuccessAction).items).toEqual(expectedUserObject);
  });
});
