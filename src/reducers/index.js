import { combineReducers } from 'redux';

import space from './space';
import user from './user';
import entries from './entries';
import assets from './assets';

export default combineReducers({
  space,
  user,
  entries,
  assets
});
