import reducer from './assets';

import {
  FETCH_SPACE_ASSETS_SUCCESS
} from '../constant/assets';

const successResponse = {
  items: [{
    fields: {
      title: 'Hero Collaboration Partial',
      contentType: 'image/png',
      fileName: 'hero-collaboration-partial.png',
      upload: 'https://wac-cdn.atlassian.com/dam/jcr:51be4df5-1ffb-4a4d-9f44-0b84dad9de5e/hero-collaboration-partial.png'
    },
    sys: {
      id: 'wtrHxeu3zEoEce2MokCSi1',
      type: 'Asset',
      version: 1,
      space: 'yadj1kx9rmg0',
      createdAt: '2015-05-18T11:29:46.809Z',
      createdBy: '4FLrUHftHW3v2BLi9fzfjU',
      updatedAt: '2015-05-18T11:29:46.809Z',
      updatedBy: '4FLrUHftHW3v2BLi9fzfjU'
    }
  }, {
    fields: {
      title: 'Stride Chat',
      contentType: 'image/png',
      fileName: 'stride_chat.svg',
      upload: 'https://wac-cdn.atlassian.com/dam/jcr:61741d76-b9a0-44b1-a31f-bfde8e6930ab/stride_chat.svg'
    },
    sys: {
      id: 'wtrHxeu3zEoEce2MokCSi2',
      type: 'Asset',
      version: 1,
      space: 'yadj1kx9rmg0',
      createdAt: '2015-05-18T11:29:46.809Z',
      createdBy: '4FLrUHftHW3v2BLi9fzfjU',
      updatedAt: '2015-05-18T11:29:46.809Z',
      updatedBy: '4FLrUHftHW3v2BLi9fzfjU2'
    }
  }
  ]
};

const expectedUserObject = [{
  contentType: 'image/png', createdBy: '4FLrUHftHW3v2BLi9fzfjU', fileName: 'hero-collaboration-partial.png', id: 'wtrHxeu3zEoEce2MokCSi1', title: 'Hero Collaboration Partial', updatedAt: '2015-05-18T11:29:46.809Z', updatedBy: '4FLrUHftHW3v2BLi9fzfjU'
}, {
  contentType: 'image/png', createdBy: '4FLrUHftHW3v2BLi9fzfjU', fileName: 'stride_chat.svg', id: 'wtrHxeu3zEoEce2MokCSi2', title: 'Stride Chat', updatedAt: '2015-05-18T11:29:46.809Z', updatedBy: '4FLrUHftHW3v2BLi9fzfjU2'
}];
describe('on call of assets reducer', () => {
  it('should set new entries', () => {
    const onSuccessAction = {
      type: FETCH_SPACE_ASSETS_SUCCESS,
      payload: successResponse
    };

    expect(reducer({}, onSuccessAction).items).toEqual(expectedUserObject);
  });
});
