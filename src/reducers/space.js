import {
  FETCH_SPACES_SUCCESS,
  FETCH_SPACES_FAILURE,
  FETCH_SPACES_BY_SPACE_ID_FAILURE,
  FETCH_SPACES_BY_SPACE_ID_SUCCESS
} from '../constant/space';

const initialState = {
  fields: [],
  spaceDataById: {},
  loading: true,
  message: undefined
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_SPACES_SUCCESS: {
      return { ...state, fields: [...payload.items], loading: !state.loading };
    }
    case FETCH_SPACES_FAILURE: {
      return { ...state, message: payload.message, loading: !state.loading };
    }
    case FETCH_SPACES_BY_SPACE_ID_FAILURE: {
      return { ...state, message: payload.message, loading: false };
    }
    case FETCH_SPACES_BY_SPACE_ID_SUCCESS: {
      return {
        ...state,
        spaceDataById: { ...state.spaceDataById, ...payload.fields },
        loading: false
      };
    }
    default: {
      return state;
    }
  }
};
