import {
  FETCH_SPACE_ENTRIES_SUCCESS
} from '../constant/entries';

const initialState = {
  items: []
};

const massageEntriesDetails = entries => entries.map((entry) => {
  const { sys, fields } = entry;

  const { title, summary } = fields;
  const {
    createdBy, updatedBy, updatedAt, id
  } = sys;

  return {
    id,
    title,
    summary,
    createdBy,
    updatedBy,
    updatedAt
  };
}, {});

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_SPACE_ENTRIES_SUCCESS: {
      return  { ...state, items: [...massageEntriesDetails(payload.items)] };
    }
    default: {
      return state;
    }
  }
};
