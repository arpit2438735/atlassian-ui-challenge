import React from 'react';

import loader from '../../icons/loading.svg';
import './style.css';

export const Loader = () => (
  <div className='loader' title='2'>
    <img src={loader} alt='loader' />
  </div>

);

export { Loader as default };
