import React from 'react';
import { shallow } from 'enzyme';

import { Link } from 'react-router-dom';

import { Panel } from './index';

let wrapper;

describe('on load of Panel', () => {
  beforeAll(() => {
    const props = {
      fields: [
        { id: 'foo', title: 'bar' }
      ]
    };

    wrapper = shallow(<Panel {...props} />);
  });

  it('should render a link', () => {
    expect(wrapper.find(Link).length).toBe(1);
  });
});
