import React from 'react';
import PropTypes from 'prop-types';

import { Row, Col } from 'react-flexbox-grid';
import { Link } from 'react-router-dom';
import './style.css';

export const Panel = ({ fields }) => {
  const template = [];

  fields.forEach((field) => {
    template.push(
      <Col xs={12} lg={12} className='nav-link' key={field.id}>
        {' '}
        <Link to={`/space/${field.id}`}>{field.title}</Link>
      </Col>
    );
  });
  return (
    <Row className='nav'>
      <Col>
        <Row>
          {template}
        </Row>
      </Col>
    </Row>
  );
};

Panel.propTypes = {
  fields: PropTypes.instanceOf(Array).isRequired
};

export { Panel as default };
