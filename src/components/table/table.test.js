import React from 'react';
import { shallow } from 'enzyme';

import BootstrapTable from 'react-bootstrap-table-next';

import { Table } from './index';

let wrapper;
let props;

describe('on load of Panel', () => {
  beforeAll(() => {
    props = {
      users: {
        '4FLrUHftHW3v2BLi9fzfjU': {
          name: 'Alana Atlassian',
          role: 'Author'
        },
        '4FLrUHftHW3v2BLi9fzfjU2': {
          name: 'John Doe',
          role: 'Editor'
        }
      },

      items:[{
        createdBy: '4FLrUHftHW3v2BLi9fzfjU', id: '5KsDBWseXY6QegucYAoacS1', summary: 'A Hello World is a common starting point for learning a new language or platform.', title: 'Hello, World!', updatedAt: '2015-05-18T11:29:46.809Z', updatedBy: '4FLrUHftHW3v2BLi9fzfjU'
      }, {
        createdBy: '4FLrUHftHW3v2BLi9fzfjU', id: '5KsDBWseXY6QegucYAoacS2', summary: 'In this tutorial, we will dive deeper into how to leverage the new platform.', title: 'Tutorial 1', updatedAt: '2015-12-18T11:29:46.809Z', updatedBy: '4FLrUHftHW3v2BLi9fzfjU2'
      }, {
        createdBy: '4FLrUHftHW3v2BLi9fzfjU', id: '5KsDBWseXY6QegucYAoacS3', summary: 'In this tutorial, we will provide an example deployment.', title: 'Tutorial 2', updatedAt: '2015-12-18T11:29:46.809Z', updatedBy: '4FLrUHftHW3v2BLi9fzfjU2'
      }]
    };

    wrapper = shallow(<Table {...props} />);
  });

  it('should render a BootstrapTable', () => {
    expect(wrapper.find(BootstrapTable).length).toBe(1);
  });

  it('should update items and add user name in place of user id', () => {
    expect(wrapper.find(BootstrapTable).props().data[0].createdBy).toContain(props.users['4FLrUHftHW3v2BLi9fzfjU'].name);
  });

  it('should set columns', () => {
    expect(wrapper.find(BootstrapTable).props().columns.length).toBe(5);
  });
});
