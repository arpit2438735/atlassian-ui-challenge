import React from 'react';
import PropTypes from 'prop-types';
import BootstrapTable from 'react-bootstrap-table-next';

import './style.css';

const ISO_8601_FULL = /^\d{4}-\d\d-\d\dT\d\d:\d\d:\d\d(\.\d+)?(([+-]\d\d:\d\d)|Z)?$/i;

const createColumns = columns => Object.keys(columns).map((column) => {
  if (column !== 'id') {
    return {
      dataField: column,
      text: column.match(/^[a-z]+|[A-Z][a-z]*/g)
        .map(x => x[0].toUpperCase() + x.substr(1).toLowerCase()).join(' '),
      sort: true,
      formatter: (cell) => {
        if (ISO_8601_FULL.test(cell)) {
          const dateObj = new Date(cell);
          return `${(`0${dateObj.getDate()}`).slice(-2)}/${(`0${dateObj.getMonth() + 1}`).slice(-2)}/${dateObj.getFullYear()}`;
        }

        return cell;
      }
    };
  }

  return undefined;
}).filter(item => item !== undefined);

const mapItemUser = (items, users) => items.map((item) => {
  if (item.createdBy) {
    item.createdBy = users[item.createdBy] ? users[item.createdBy].name : item.createdBy;
  }

  if (item.updatedBy) {
    item.updatedBy = users[item.updatedBy] ? users[item.updatedBy].name : item.updatedBy;
  }

  return item;
});

export const Table = ({ items, users }) => (
  <BootstrapTable keyField='id' columns={createColumns(items[0])} data={mapItemUser(items, users)} />
);

Table.propTypes = {
  items: PropTypes.instanceOf(Array).isRequired,
  users: PropTypes.instanceOf(Object).isRequired
};

export { Table as default };
