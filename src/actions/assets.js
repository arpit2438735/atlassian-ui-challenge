import {
  FETCH_SPACE_ASSETS_FAILURE,
  FETCH_SPACE_ASSETS_SUCCESS
} from '../constant/assets';

const fetchAssetsForSpace = () => async (dispatch) => {
  const response = await fetch('/api/assets');

  if (!response.ok && response.status >= 500) {
    return dispatch({
      type: FETCH_SPACE_ASSETS_FAILURE,
      payload: {
        message: 'Some issue in the internet',
        success: false
      }
    });
  }

  const data = await response.json();

  if (data.message) {
    return dispatch({
      type: FETCH_SPACE_ASSETS_FAILURE,
      payload: {
        message: data.message,
        success: false
      }
    });
  }

  return dispatch({
    type: FETCH_SPACE_ASSETS_SUCCESS,
    payload: data
  });
};

export { fetchAssetsForSpace as default };
