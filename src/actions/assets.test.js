import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import {
  FETCH_SPACE_ASSETS_SUCCESS,
  FETCH_SPACE_ASSETS_FAILURE
} from '../constant/assets';

import fetchAssetsForSpace from './assets';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const successData = [{ fields: { title: 'bar' } }];
const failureData = {
  message: 'not found'
};

describe('on call of assets api', () => {
  afterEach(() => {
    fetchMock.reset();
    fetchMock.restore();
  });

  describe('on getting success response from api', () => {
    beforeAll(() => {
      fetchMock
        .getOnce('/api/assets', { body: successData, headers: { 'content-type': 'application/json' } });
    });

    it('should dispatch success event', () => {
      const store = mockStore({ assets: {} });

      const expectedActions = [
        { type: FETCH_SPACE_ASSETS_SUCCESS, payload: successData }
      ];

      return store.dispatch(fetchAssetsForSpace()).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });

  describe('on getting failure response from api', () => {
    beforeAll(() => {
      fetchMock
        .getOnce('/api/assets', { status: 400, body: failureData, headers: { 'content-type': 'application/json' } });
    });

    it('should dispatch success event', () => {
      const store = mockStore({ assets: {} });

      const expectedActions = [
        { type: FETCH_SPACE_ASSETS_FAILURE, payload: { ...failureData, success: false } }
      ];

      return store.dispatch(fetchAssetsForSpace()).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
});
