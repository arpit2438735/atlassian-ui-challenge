import {
  FETCH_SPACE_ENTRIES_FAILURE,
  FETCH_SPACE_ENTRIES_SUCCESS
} from '../constant/entries';

const fetchEntriesForSpace = () => async (dispatch) => {
  const response = await fetch('/api/entries');

  if (!response.ok && response.status >= 500) {
    return dispatch({
      type: FETCH_SPACE_ENTRIES_FAILURE,
      payload: {
        message: 'Some issue in the internet',
        success: false
      }
    });
  }

  const data = await response.json();

  if (data.message) {
    return dispatch({
      type: FETCH_SPACE_ENTRIES_FAILURE,
      payload: {
        message: data.message,
        success: false
      }
    });
  }

  return dispatch({
    type: FETCH_SPACE_ENTRIES_SUCCESS,
    payload: data
  });
};

export { fetchEntriesForSpace as default };
