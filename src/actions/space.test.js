import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import {
  FETCH_SPACES_FAILURE,
  FETCH_SPACES_SUCCESS,
  FETCH_SPACES_BY_SPACE_ID_SUCCESS,
  FETCH_SPACES_BY_SPACE_ID_FAILURE
} from '../constant/space';

import {
  fetchAllSpace,
  fetchSpaceBySpaceId
} from './space';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const successData = [{ foo: 'bar' }];
const failureData = {
  message: 'not found'
};
const spaceId = 'foo';

describe('on call of space api', () => {
  afterEach(() => {
    fetchMock.reset();
    fetchMock.restore();
  });

  describe('on getting success response from api', () => {
    beforeAll(() => {
      fetchMock
        .getOnce('/api/space', { body: successData, headers: { 'content-type': 'application/json' } });
    });

    it('should dispatch success event', () => {
      const store = mockStore({ space: {} });

      const expectedActions = [
        { type: FETCH_SPACES_SUCCESS, payload: successData }
      ];

      return store.dispatch(fetchAllSpace()).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });

  describe('on getting failure response from api', () => {
    beforeAll(() => {
      fetchMock
        .getOnce('/api/space', { status: 400, body: failureData, headers: { 'content-type': 'application/json' } });
    });

    it('should dispatch success event', () => {
      const store = mockStore({ space: {} });

      const expectedActions = [
        { type: FETCH_SPACES_FAILURE, payload: { ...failureData, success: false } }
      ];

      return store.dispatch(fetchAllSpace()).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });

  describe('on getting success response from field by id api', () => {
    beforeAll(() => {
      fetchMock
        .getOnce(`/api/space/${spaceId}`, { body: successData, headers: { 'content-type': 'application/json' } });
    });

    it('should dispatch success event', () => {
      const store = mockStore({ space: {} });

      const expectedActions = [
        { type: FETCH_SPACES_BY_SPACE_ID_SUCCESS, payload: successData }
      ];

      return store.dispatch(fetchSpaceBySpaceId({ spaceId })).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });

  describe('on getting failure response from field by id api', () => {
    beforeAll(() => {
      fetchMock
        .getOnce(`/api/space/${spaceId}`, { status: 400, body: failureData, headers: { 'content-type': 'application/json' } });
    });

    it('should dispatch success event', () => {
      const store = mockStore({ space: {} });

      const expectedActions = [
        { type: FETCH_SPACES_BY_SPACE_ID_FAILURE, payload: { ...failureData, success: false } }
      ];

      return store.dispatch(fetchSpaceBySpaceId({ spaceId })).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
});
