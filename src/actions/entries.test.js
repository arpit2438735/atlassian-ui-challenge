import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import {
  FETCH_SPACE_ENTRIES_SUCCESS,
  FETCH_SPACE_ENTRIES_FAILURE
} from '../constant/entries';

import fetchEntriesForSpace from './entries';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const successData = [{ fields: { title: 'bar' } }];
const failureData = {
  message: 'not found'
};

describe('on call of entries api', () => {
  afterEach(() => {
    fetchMock.reset();
    fetchMock.restore();
  });

  describe('on getting success response from api', () => {
    beforeAll(() => {
      fetchMock
        .getOnce('/api/entries', { body: successData, headers: { 'content-type': 'application/json' } });
    });

    it('should dispatch success event', () => {
      const store = mockStore({ entries: {} });

      const expectedActions = [
        { type: FETCH_SPACE_ENTRIES_SUCCESS, payload: successData }
      ];

      return store.dispatch(fetchEntriesForSpace()).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });

  describe('on getting failure response from api', () => {
    beforeAll(() => {
      fetchMock
        .getOnce('/api/entries', { status: 400, body: failureData, headers: { 'content-type': 'application/json' } });
    });

    it('should dispatch success event', () => {
      const store = mockStore({ entries: {} });

      const expectedActions = [
        { type: FETCH_SPACE_ENTRIES_FAILURE, payload: { ...failureData, success: false } }
      ];

      return store.dispatch(fetchEntriesForSpace()).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
});
