import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import {
  FETCH_USERS_DETAILS_FAILURE,
  FETCH_USER_DETAILS_SUCCESS
} from '../constant/user';

import fetchUserDetails from './user';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const successData = [{ fields: { name: 'bar' } }];
const failureData = {
  message: 'not found'
};

describe('on call of user api', () => {
  afterEach(() => {
    fetchMock.reset();
    fetchMock.restore();
  });

  describe('on getting success response from api', () => {
    beforeAll(() => {
      fetchMock
        .getOnce('/api/users', { body: successData, headers: { 'content-type': 'application/json' } });
    });

    it('should dispatch success event', () => {
      const store = mockStore({ user: {} });

      const expectedActions = [
        { type: FETCH_USER_DETAILS_SUCCESS, payload: successData }
      ];

      return store.dispatch(fetchUserDetails()).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });

  describe('on getting failure response from api', () => {
    beforeAll(() => {
      fetchMock
        .getOnce('/api/users', { status: 400, body: failureData, headers: { 'content-type': 'application/json' } });
    });

    it('should dispatch success event', () => {
      const store = mockStore({ user: {} });

      const expectedActions = [
        { type: FETCH_USERS_DETAILS_FAILURE, payload: { ...failureData, success: false } }
      ];

      return store.dispatch(fetchUserDetails()).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
});
