import {
  FETCH_SPACES_FAILURE,
  FETCH_SPACES_SUCCESS,
  FETCH_SPACES_BY_SPACE_ID_FAILURE,
  FETCH_SPACES_BY_SPACE_ID_SUCCESS
} from '../constant/space';

export const fetchAllSpace = () => async (dispatch) => {
  const response = await fetch('/api/space');

  if (!response.ok && response.status >= 500) {
    return dispatch({
      type: FETCH_SPACES_FAILURE,
      payload: {
        message: 'Some issue in the internet',
        success: false
      }
    });
  }

  const data = await response.json();

  if (data.message) {
    return dispatch({
      type: FETCH_SPACES_FAILURE,
      payload: {
        message: data.message,
        success: false
      }
    });
  }

  return dispatch({
    type: FETCH_SPACES_SUCCESS,
    payload: data
  });
};

export const fetchSpaceBySpaceId = ({ spaceId }) => async (dispatch) => {
  const response = await fetch(`/api/space/${spaceId}`);

  if (!response.ok && response.status >= 500) {
    return dispatch({
      type: FETCH_SPACES_BY_SPACE_ID_FAILURE,
      payload: {
        message: 'Some issue in the internet',
        success: false
      }
    });
  }

  const data = await response.json();

  if (data.message) {
    return dispatch({
      type: FETCH_SPACES_BY_SPACE_ID_FAILURE,
      payload: {
        message: data.message,
        success: false
      }
    });
  }

  return dispatch({
    type: FETCH_SPACES_BY_SPACE_ID_SUCCESS,
    payload: data
  });
};
