import {
  FETCH_USER_DETAILS_SUCCESS,
  FETCH_USERS_DETAILS_FAILURE
} from '../constant/user';

const fetchUserDetails = () => async (dispatch) => {
  const response = await fetch('/api/users');

  if (!response.ok && response.status >= 500) {
    return dispatch({
      type: FETCH_USERS_DETAILS_FAILURE,
      payload: {
        message: 'Some issue in the internet',
        success: false
      }
    });
  }

  const data = await response.json();

  if (data.message) {
    return dispatch({
      type: FETCH_USERS_DETAILS_FAILURE,
      payload: {
        message: data.message,
        success: false
      }
    });
  }

  return dispatch({
    type: FETCH_USER_DETAILS_SUCCESS,
    payload: data
  });
};

export { fetchUserDetails as default };
