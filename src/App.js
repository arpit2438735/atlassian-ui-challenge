import React from 'react';
import { Route } from 'react-router-dom';

import Home from './pages/home';
import Space from './pages/space';
import NotFound from './pages/404';

const App = () => (
    <div>
        <Route exact path="/" component={Home} />
        <Route exact path="/space" component={Space} />
        <Route exact path="/space/:spaceId" component={Space} />
        <Route path="*" component={NotFound}/>
    </div>
);

export default App;
